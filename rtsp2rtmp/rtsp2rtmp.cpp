#include "rtsp2rtmp.h"
#include "rtsp2rtmpimpl.h"
#include "loghelper.h"

using namespace std;
using namespace RockLog;

void Rtsp2Rtmp::startPlay(const Rtsp2RtmpReq_t &req, int rtmpMode)
{
    std::map<std::string, Rtsp2RtmpImpl *>::iterator iter = _rtsp2Rtmps.find(req.node);
    if (iter != _rtsp2Rtmps.end())
    {
        if (iter->second)
        {
            // 开始播放视频流
            iter->second->setPlayCount(true);
        }
        else
        {
            _rtsp2Rtmps.erase(iter);
        }
        return;
    }

    // 开始推流
    Rtsp2RtmpImpl *rtsp2rtmp = new Rtsp2RtmpImpl(req, rtmpMode);

    rtsp2rtmp->_uninitSignal.connect(std::bind(&Rtsp2Rtmp::deleteNode, this, std::placeholders::_1));
    std::thread rtsp2rtmpThread(&Rtsp2RtmpImpl::start, rtsp2rtmp);
    rtsp2rtmpThread.detach();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));

    bool flag = rtsp2rtmp->getRtsp2RtmpFlag();
    if (flag)
    {
        //推流成功
        LOG(kInfo) << "rtsp to rtmp success, the node is " << req.node << std::endl;
        _rtsp2Rtmps.insert(std::pair<std::string, Rtsp2RtmpImpl *>(req.node, rtsp2rtmp));
    }
    else
    {
        //推流失败
        LOG(kInfo) << "rtsp to rtmp fail, the node is " << req.node << std::endl;
        rtsp2rtmp = nullptr;
    }
}

// 停止推流
void Rtsp2Rtmp::stopPlay(const std::string &node)
{
    std::map<std::string, Rtsp2RtmpImpl *>::iterator iter = _rtsp2Rtmps.find(node);
    if (iter != _rtsp2Rtmps.end())
    {
        if (iter->second)
        {
            // 结束播放视频流
            int count = iter->second->setPlayCount(false);
            if (0 == count)
            {
                LOG(kDebug) << "the play count is 0, will shut down rtsp to rtmp, the node is " << node << std::endl;
                iter->second->setRtsp2RtmpFlag(false);
                iter->second = nullptr;
                _rtsp2Rtmps.erase(iter);
            }
        }
        else
        {
            _rtsp2Rtmps.erase(iter);
        }
    }
}

void Rtsp2Rtmp::shutDown(const std::string &node)
{
    std::map<std::string, Rtsp2RtmpImpl *>::iterator iter = _rtsp2Rtmps.find(node);
    if (iter != _rtsp2Rtmps.end())
    {
        if (iter->second)
        {
            //停止推流
            iter->second->setRtsp2RtmpFlag(false);
            iter->second = nullptr;
            _rtsp2Rtmps.erase(iter);
        }
        else
        {
            _rtsp2Rtmps.erase(iter);
        }
    }
}

void Rtsp2Rtmp::deleteNode(std::string node)
{
    LOG(kDebug) << "delete node:" << node << endl;

    std::map<std::string, Rtsp2RtmpImpl *>::iterator iter = _rtsp2Rtmps.find(node);
    if (iter != _rtsp2Rtmps.end())
    {
        if (iter->second)
        {
            delete iter->second;
            _rtsp2Rtmps.erase(iter);
        }
        else
        {
            _rtsp2Rtmps.erase(iter);
        }
    }
}
