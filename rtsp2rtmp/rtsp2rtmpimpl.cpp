#include "rtsp2rtmpimpl.h"
#include "loghelper.h"

using namespace std;
using namespace RockLog;

// ref:https://blog.csdn.net/yao_hou/article/details/103267569
// ref:https://www.cnblogs.com/wanggang123/p/7222215.html
Rtsp2RtmpImpl::Rtsp2RtmpImpl(const Rtsp2RtmpReq_t &req, int rtmpMode)
    : _isWorking(false),
      _isInited(false),
      _playCount(0),
      _node(req.node),
      _inputUrl(req.rtspUrl),
      _outUrl(req.rtmpUrl),
      _rtmpMode(rtmpMode)
{
    init();
}

Rtsp2RtmpImpl::~Rtsp2RtmpImpl()
{
    unInit();
}

void Rtsp2RtmpImpl::init()
{
    av_register_all();
    avfilter_register_all();
    avformat_network_init();
    av_log_set_level(AV_LOG_ERROR);

    LOG(kInfo) << "rtsp url: " << _inputUrl << std::endl;
    LOG(kInfo) << "rtmp url: " << _outUrl << std::endl;

    int ret = 0;
    if (_rtmpMode)
    {
        LOG(kDebug) << "open input url by tcp" << std::endl;
        ret = openInputByTcp(_inputUrl);
    }
    else
    {
        LOG(kDebug) << "open input url by udp" << std::endl;
        ret = openInputByUdp(_inputUrl);
    }

    if (ret >= 0)
    {
        _isInited = true;
        ret = openOutput(_outUrl);
        _isWorking = true;
    }

    if (ret < 0)
    {
        //取流或者转流失败
        _isWorking = false;
        _playCount = 0;
    }
}

void Rtsp2RtmpImpl::unInit()
{
    _isWorking = false;
    if (_isInited)
    {
        if (_outputContext)
        {
            LOG(kDebug) << "close output url" << std::endl;
            avformat_close_input(&_outputContext);
        }
        if (_inputContextTcp)
        {
            LOG(kDebug) << "close tcp input url" << std::endl;
            avformat_close_input(&_inputContextTcp);
        }

        if (_inputContextUdp)
        {
            LOG(kDebug) << "close udp input url" << std::endl;
            avformat_close_input(&_inputContextUdp);
        }
        _isInited = false;
    }
}

int Rtsp2RtmpImpl::setPlayCount(bool isPlay)
{
    if (isPlay)
    {
        _playCount++;
        LOG(kDebug) << "after play, the play count: " << _playCount << ", the node is " << _node << std::endl;
    }
    else
    {
        if (_playCount > 0)
        {
            _playCount--;
            LOG(kDebug) << "after play done, the play count: " << _playCount << ", the node is " << _node << std::endl;
        }
    }

    return _playCount;
}

bool Rtsp2RtmpImpl::getRtsp2RtmpFlag()
{
    return _isWorking;
}

void Rtsp2RtmpImpl::setRtsp2RtmpFlag(bool flag)
{
    LOG(kDebug) << "set the Rtsp2RtmpImpl flag to " << flag << ", the node is " << _node << std::endl;
    _isWorking = flag;
}

void Rtsp2RtmpImpl::start()
{
    do
    {
        int64_t count = 0;
        while (_isWorking)
        {
            shared_ptr<AVPacket> packet = nullptr;

            if (_rtmpMode)
            {
                //LOG(kDebug) << "read packet by tcp" << std::endl;
                packet = readPacketFromSourceByTcp();
            }
            else
            {
                //LOG(kDebug) << "read packet by udp" << std::endl;
                packet = readPacketFromSourceByUdp();
            }

            if (packet)
            {
                int ret = writePacket(packet);

                //	this_thread::sleep_for(std::chrono::seconds(5000));
                if (ret >= 0)
                {
                    count++;
                    if ((count % 200) == 0)
                        cout << count << " WritePacket Success! node: " << _node << endl;
                }
                else if (ret < 0)
                {
                    cout << "WritePacket failed! ret: " << ret << ", node: " << _node << endl;
                }
            }
            else
            {
                this_thread::sleep_for(chrono::milliseconds(10));
            }
        }
        unInit();
        LOG(kDebug) << "shut down Rtsp2RtmpImpl, node: " << _node << endl;
        _uninitSignal(_node);

    } while (0);
}

int Rtsp2RtmpImpl::openInputByTcp(const string &inputUrl)
{
    _inputContextTcp = avformat_alloc_context();
    AVDictionary *options = nullptr;
    av_dict_set(&options, "rtsp_transport", "tcp", 0);
    av_dict_set(&options, "stimeout", "5000000", 0);

    int ret = avformat_open_input(&_inputContextTcp, inputUrl.c_str(), nullptr, &options);
    if (ret < 0)
    {
        LOG(kErr) << "open input url " << inputUrl << " failed" << std::endl;
        return ret;
    }

    ret = avformat_find_stream_info(_inputContextTcp, nullptr);
    if (ret < 0)
    {
        LOG(kErr) << "find input url stream information failed" << std::endl;
    }
    else
    {
        LOG(kDebug) << "open input url " << inputUrl << " success" << std::endl;
    }

    return ret;
}

int Rtsp2RtmpImpl::openInputByUdp(const string &inputUrl)
{
    _inputContextUdp = avformat_alloc_context();
    AVDictionary *options = nullptr;
    av_dict_set(&options, "rtsp_transport", "udp", 0);
    av_dict_set(&options, "stimeout", "5000000", 0);

    int ret = avformat_open_input(&_inputContextUdp, inputUrl.c_str(), nullptr, &options);
    if (ret < 0)
    {
        LOG(kErr) << "open input url " << inputUrl << " failed" << std::endl;
        return ret;
    }

    ret = avformat_find_stream_info(_inputContextUdp, nullptr);
    if (ret < 0)
    {
        LOG(kErr) << "find input url stream information failed" << std::endl;
    }
    else
    {
        LOG(kDebug) << "open input url " << inputUrl << " success" << std::endl;
    }

    return ret;
}

shared_ptr<AVPacket> Rtsp2RtmpImpl::readPacketFromSourceByTcp()
{
    shared_ptr<AVPacket> packet(
        static_cast<AVPacket *>(av_malloc(sizeof(AVPacket))),
        [&](AVPacket *p) { av_packet_free(&p); av_freep(&p); });

    av_init_packet(packet.get());

    int ret = av_read_frame(_inputContextTcp, packet.get());
    if (ret >= 0)
    {
        return packet;
    }
    else
    {
        return nullptr;
    }
}

shared_ptr<AVPacket> Rtsp2RtmpImpl::readPacketFromSourceByUdp()
{
    shared_ptr<AVPacket> packet(
        static_cast<AVPacket *>(av_malloc(sizeof(AVPacket))),
        [&](AVPacket *p) { av_packet_free(&p); av_freep(&p); });

    av_init_packet(packet.get());

    int ret = av_read_frame(_inputContextUdp, packet.get());
    if (ret >= 0)
    {
        return packet;
    }
    else
    {
        return nullptr;
    }
}

void Rtsp2RtmpImpl::av_packet_rescale_ts(AVPacket *pkt, AVRational src_tb, AVRational dst_tb)
{
    if (pkt->pts != AV_NOPTS_VALUE)
        pkt->pts = av_rescale_q(pkt->pts, src_tb, dst_tb);

    if (pkt->dts != AV_NOPTS_VALUE)
        pkt->dts = av_rescale_q(pkt->dts, src_tb, dst_tb);

    if (pkt->duration > 0)
        pkt->duration = av_rescale_q(pkt->duration, src_tb, dst_tb);
}

int Rtsp2RtmpImpl::writePacket(shared_ptr<AVPacket> packet)
{
    auto outputStream = _outputContext->streams[packet->stream_index];

    if (_inputContextTcp)
    {
        auto inputStream = _inputContextTcp->streams[packet->stream_index];

        av_packet_rescale_ts(packet.get(), inputStream->time_base, outputStream->time_base);
    }
    else if (_inputContextUdp)
    {
        auto inputStream = _inputContextUdp->streams[packet->stream_index];

        av_packet_rescale_ts(packet.get(), inputStream->time_base, outputStream->time_base);
    }

    return av_interleaved_write_frame(_outputContext, packet.get());
}

int Rtsp2RtmpImpl::openOutput(const string &outUrl)
{
    int ret = 0;

    do
    {
        ret = avformat_alloc_output_context2(&_outputContext, nullptr, "flv", outUrl.c_str());
        if (ret < 0)
        {
            LOG(kErr) << "open output context failed" << std::endl;
            goto Error;
        }

        ret = avio_open2(&_outputContext->pb, outUrl.c_str(), AVIO_FLAG_READ_WRITE, nullptr, nullptr);
        if (ret < 0)
        {
            LOG(kErr) << "open avio failed" << std::endl;
            goto Error;
        }

        if (_inputContextTcp)
        {
            for (int i = 0; i < _inputContextTcp->nb_streams; i++)
            {
                AVStream *stream = avformat_new_stream(_outputContext, _inputContextTcp->streams[i]->codec->codec);
                ret = avcodec_copy_context(stream->codec, _inputContextTcp->streams[i]->codec);
                if (ret < 0)
                {
                    LOG(kErr) << "copy coddec context failed" << std::endl;
                    goto Error;
                }
            }
        }
        else if (_inputContextUdp)
        {
            for (int i = 0; i < _inputContextUdp->nb_streams; i++)
            {
                AVStream *stream = avformat_new_stream(_outputContext, _inputContextUdp->streams[i]->codec->codec);
                ret = avcodec_copy_context(stream->codec, _inputContextUdp->streams[i]->codec);
                if (ret < 0)
                {
                    LOG(kErr) << "copy coddec context failed" << std::endl;
                    goto Error;
                }
            }
        }

        ret = avformat_write_header(_outputContext, nullptr);
        if (ret < 0)
        {
            LOG(kErr) << "format write header failed" << std::endl;
            goto Error;
        }

        LOG(kDebug) << "open output url " << outUrl << " success" << std::endl;

        return ret;

    } while (0);

Error:
    if (_outputContext)
    {
        for (int i = 0; i < _outputContext->nb_streams; i++)
        {
            avcodec_close(_outputContext->streams[i]->codec);
        }

        avformat_close_input(&_outputContext);
    }

    return ret;
}
