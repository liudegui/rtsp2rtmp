# rtsp2rtmp

+ 使用ffmpeg将rtsp码流转成rtmp码流；
+ 支持tcp和udp；
+ 内部统计了播放次数，以减少重复推流;
+ 使用ffmpeg 3.2.4和boost1.66编译正常。

---------

+ Use ffmpeg to convert RTSP to RTMP;
+ Support TCP and UDP;
+ Count the number of plays to reduce repeated streaming；
+ ffmpeg 3.2.4 version and boost 1.66 works for me.


