#include "rtsp2rtmp.h"
#include <iostream>
using namespace std;

int main(int argc, const char **argv)
{
    if (argc < 3)
    {
        std::cout << "Usage ./rtsp2rtmp [rtsp:url] [rtmp:url]" << std::endl;
        return -1;
    }

    std::string rtsp_url(argv[1]);
    std::string rtmp_url(argv[2]);

    std::cout << "rtsp url " << rtsp_url << std::endl;
    std::cout << "rtmp url " << rtmp_url << std::endl;

    Rtsp2Rtmp nodeManager;
    Rtsp2RtmpReq_t req;
    req.node = "123";
    req.rtspUrl = rtsp_url;
    req.rtmpUrl = rtmp_url;
    // req.rtspUrl = "rtsp://192.21.1.37:554/rgb_0.xipc/real?";
    // req.rtmpUrl = "rtmp://192.21.1.43:1935/rock/mystream";

    nodeManager.startPlay(req, 0);

    char c(' ');
    while (c != 'q' && c != 'Q')
    {
        std::cout << "Press q then enter to quit: \n";
        std::cin >> c;
    }

    return 0;
}