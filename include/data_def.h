
#pragma once

#include <string>
struct Rtsp2RtmpReq_t
{
    std::string node;
    std::string rtspUrl;
    std::string rtmpUrl;
};