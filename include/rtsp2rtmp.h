
#pragma once

#if (!defined(ROCK_LIB) && !defined(ROCK_LINUX_PLATFORM))
// ROCK is used as DLL
    #define ROCK_DLLEXPORT __declspec(dllexport) 
    #define ROCK_DLLIMPORT __declspec(dllimport) 
#else 
    #define ROCK_DLLEXPORT 
    #define ROCK_DLLIMPORT
#endif

#ifdef ROCK_RTSP2RTMP_EXPORTS
    #define ROCK_RTSP2RTMP_API ROCK_DLLEXPORT  
#else 
    #define ROCK_RTSP2RTMP_API ROCK_DLLIMPORT 
#endif


#include <string>
#include <map>
#include "data_def.h"

class Rtsp2RtmpImpl;
class ROCK_RTSP2RTMP_API Rtsp2Rtmp
{
public:
    Rtsp2Rtmp() = default;
    ~Rtsp2Rtmp() = default;

    void startPlay(const Rtsp2RtmpReq_t &req, int rtmpMode);
    void stopPlay(const std::string &node);
    void shutDown(const std::string &node);

private:
    void deleteNode(std::string node);
    std::map<std::string, Rtsp2RtmpImpl *> _rtsp2Rtmps;
};