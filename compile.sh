#!/bin/bash

build="build"
if [ ! -d $build ]; then
	mkdir $build
else
	rm -rf $build/*
fi

cd $build

cmake "-DCMAKE_BUILD_TYPE=Release"  ..

make -j8

make install

cd ..
